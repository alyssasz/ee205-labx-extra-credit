///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file eval.cpp
/// @version 1.0
///
/// Evaluate a number of Standard C++ Collection classes for performance
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   May 11 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <cstring>
#include <string>
#include <cmath>
#include <x86intrin.h>
#include <limits.h>

#include "testClass.cpp"

using namespace std;

///////////////////////////////////  MAIN  ///////////////////////////////////
int main() {
	cout << "Welcome to the Gnu C++ Collection Class Evaluator" << endl;

	static long start, end;
	for( unsigned int i = 0 ; i < CALIBRATE_OVERHEAD ; i++ ) {
		MARK_TIME( start );
		MARK_TIME( end );
		TEST_OVERHEAD = min( TEST_OVERHEAD, (end - start) );
	}

	cout << "Approximate test overhead is: " << TEST_OVERHEAD << endl;

	// Implement your Collection Class Evaluator here
	ABSTRACT_TEST_CLASS* TestClasses[6]; //Array of pointers to the base class
	TestVector Vector;
	TestList List;
	TestSet Set;
	TestMap Map;
	TestUnMap UnMap;
	TestUnSet UnSet;
	

	TestClasses[0] = &Vector; //Saving location of Derived Class as a Pointer to the base class 
	TestClasses[1] = &List;
	TestClasses[2] = &Set;
	TestClasses[3] = &Map;
	TestClasses[4] = &UnMap;
	TestClasses[5] = &UnSet;
	

	for(int i = 0; i != 6; i++){
		Result* TResult = new Result(TestClasses[i], TestClasses[i]->name()) ;
		TResult->testDataStructure();
		if(i == 0)
			TResult->printResultsHeader();
		TResult->printResults();
		delete TResult;
	}


} // main()



